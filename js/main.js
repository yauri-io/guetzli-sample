$(function () {

    $(document).foundation();

    $(document)
        .on("invalid.zf.abide", function (ev, elem) {
            if (ev.target.id === 'imageUrl') {
                $("#btnSubmit").prop('disabled', true);
            }
        })
        .on("valid.zf.abide", function (ev, elem) {
            if (ev.target.id === 'imageUrl') {
                $("#btnSubmit").prop('disabled', false);
            }
        });

    $("#imageUrl").change( function (e) {
        if (!e.target.value) {
            $("#btnSubmit").prop('disabled', true);
        }
    });

    $("#btnSubmit").on("click", function (e) {

        e.preventDefault();
        const url = $("#imageUrl").val();
        $("#loadingContainer").show();
        $("#imageContainer").hide();
        $("#btnSubmit").prop('disabled', true);

        $.ajax({
            type: "POST",
            url: "upload-url.php",
            data: JSON.stringify({url: url}),
            error: function(){
                alert("Sorry there is an error on the convert process");
            },
            success: function(res) {
                const baseUrl = window.location.toString();

                $("#originalImage").attr("src", baseUrl + res.originalImage);
                $("#originalImageLink").attr({
                    'href': baseUrl + res.originalImage,
                    'target': "_blank"
                });
                $("#convertedImage").attr("src", baseUrl + res.convertedImage);
                $("#convertedImageLink").attr({
                    'href': baseUrl + res.convertedImage,
                    "target": "_blank"
                });
            },
            complete: function() {
                $("#loadingContainer").hide();
                $("#imageContainer").show();
                $("#btnSubmit").prop('disabled', false);
            },
            contentType: "application/json",
            dataType: "json"
        });
    });
});