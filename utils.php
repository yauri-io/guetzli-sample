<?php
/**
 * Created by PhpStorm.
 * User: euthenia
 * Date: 4/2/17
 * Time: 8:44 PM
 */

date_default_timezone_set('Europe/London');

function getImageNameFromUrl($url) {
    return basename($url);
}

function downloadImage($url, $imgName)
{
    $imgLocation = __DIR__ . "/temp-image/" . $imgName;
    file_put_contents($imgLocation, file_get_contents($url));

    $ch = curl_init($url);
    $fp = fopen($imgLocation, "wb");
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    return "/temp-image/" . $imgName;
}

function responseJSON($responseArray) {
    header("Content-Type: application/json");
    echo json_encode($responseArray);
}

function convertImage($imageName, $quality = 90) {

    $newImageName = gettimeofday()['sec'] . $imageName;
    $imageLocationOri = __DIR__ . "/temp-image/" . $imageName;
    $imageLocationCopy = __DIR__ . "/con-image/" . $newImageName;
    exec("guetzli --quality $quality $imageLocationOri $imageLocationCopy");
    return "/con-image/$newImageName";
}
