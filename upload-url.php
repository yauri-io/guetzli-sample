<?php
/**
 * Created by PhpStorm.
 * User: euthenia
 * Date: 4/2/17
 * Time: 8:24 PM
 */

require_once('./utils.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $input = file_get_contents('php://input');
    $body = json_decode($input, true);

    if (!empty($body["url"])) {
        $imgName = gettimeofday()['sec'] . getImageNameFromUrl($body["url"]);
        $originalImage = downloadImage($body["url"], $imgName);
        $convertedImage = convertImage($imgName);
        responseJSON(array(
            "originalImage" => $originalImage,
            "convertedImage" => $convertedImage
        ));
    } else {
        responseJSON(array(
            "error" => "Invalid image URL"
        ));
    }
}