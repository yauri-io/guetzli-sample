<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Guetzli Test</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="top-bar">
    <div class="row">
        <div class="columns">
            <div class="top-bar-left text-center">
                Guetzli JPEG Encoder <a href="https://github.com/google/guetzli">(Github)</a>
                <a target="_blank" href="https://research.googleblog.com/2017/03/announcing-guetzli-new-open-source-jpeg.html">
                    <h5>Google research blog about the encoder</h5>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="vertical-space"></div>
<div class="warning callout">
    Because of server limitation, the image quality after compression is 90. Max is 100 and min is 84.<br>
    Some URL may not work.
</div>
<div class="row">
    <div class="columns">
        <form data-abide novalidate>
            <div class="input-group">
                <span class="input-group-label">URL</span>
                <input id="imageUrl" class="input-group-field" type="text"
                       placeholder="http://www.example.com/image.jpg" pattern="url">
                <div class="input-group-button">
                    <input type="submit" class="button" id="btnSubmit" value="Submit" disabled>
                </div>
            </div>
        </form>
    </div>
</div>

<!--<div class="row">-->
<!--    <div class="column text-center">-->
<!--        <h3>OR</h3>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="row">-->
<!--    <div class="columns">-->
<!--        <form>-->
<!--            <div class="input-group align-center">-->
<!--                <label for="imageUpload" class="button">Upload Image</label>-->
<!--                <input type="file" id="imageUpload" id="btnUpload" class="show-for-sr">-->
<!--            </div>-->
<!--        </form>-->
<!--    </div>-->
<!--</div>-->

<div class="vertical-space"></div>

<div class="row">
    <div id="loadingContainer" class="small-12 columns text-center" style="display:none">
        <h4>Please wait, converting</h4>
        <div id="spinningSquaresG">
            <div id="spinningSquaresG_1" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_2" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_3" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_4" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_5" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_6" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_7" class="spinningSquaresG"></div>
            <div id="spinningSquaresG_8" class="spinningSquaresG"></div>
        </div>
    </div>
</div>

<div class="row" id="imageContainer" style="display:none">
    <div class="small-6 columns text-center">
        <h4>Original</h4>
        <a id="originalImageLink" href="">
            <img class="thumbnail" id="originalImage" src="https://placehold.it/650x350">
        </a>
    </div>
    <div class="small-6 columns text-center">
        <h4>Compressed</h4>
        <a id="convertedImageLink" href="">
            <img class="thumbnail" id="convertedImage" src="https://placehold.it/650x350">
        </a>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.0/js/foundation.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-96633453-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
